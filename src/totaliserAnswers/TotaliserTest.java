package totaliserAnswers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import totaliserQuestion.Totaliser;

public class TotaliserTest {
	
	private Totaliser totaliser;
	final int SOME_NUM = 10;
	final int SOME_NEG = -10;
	final int ZERO_NUM = 0;
	
	@Before
	public void setup() {
		totaliser = new Totaliser();
	}

	@Test
	public void zeroSumAfterConstructor() {
		assertEquals(totaliser.getSum(), 0);
		//assertTrue(totaliser.getSum() == 0);
	}
	
	
	@Test
	public void zeroSumAfterReset() {
		totaliser.enterValue(SOME_NUM);
		totaliser.reset();
		assertEquals(totaliser.getSum(), ZERO_NUM);
	}
	
		
	@Test
	public void enterOneValue() {
		totaliser.enterValue(SOME_NUM);
		assertEquals(SOME_NUM, totaliser.getSum());
	}
	
	@Test
	public void enterMultipleValues() {
		totaliser.enterValue(SOME_NUM);
		totaliser.enterValue(SOME_NUM);
		totaliser.enterValue(SOME_NUM);
		
		assertEquals(3*SOME_NUM, totaliser.getSum());
	}
	
	@Test
	public void negativeEnterValue() {
		totaliser.enterValue(SOME_NEG);
		totaliser.enterValue(SOME_NEG);
		
		assertEquals(2*SOME_NEG, totaliser.getSum());
	}
	
	

}

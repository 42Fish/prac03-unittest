package allSameAnswers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import allSameQuestion.AllSameA;
import allSameQuestion.AllSameB;
import allSameQuestion.AllSameC;


public class AllSameTest {
	
	private AllSameA allSameA;
	private AllSameB allSameB;
	private AllSameC allSameC;
	
	private int[] emptyArray = {};
	private int[] negArray = {-1,-2,-3};
	private int[] manyArrayTrue = {3, 3, 3, 3, 3, 3, 3};
	private int[] manyArrayFalse = {3, 1, 1};
	private int[] manyNormal = {2, 1, 3};
	private int[] boundaryEnd = {5, 5, 5, 5, 4};
	private int[] boundaryBegin = {4, 5, 5, 5, 5};
	@Before
	public void setup(){
		allSameA = new AllSameA();
		allSameB = new AllSameB();
		allSameC = new AllSameC();
	}

	@Test(expected=Exception.class)
	public void notEmptyA() throws Exception {
		assertTrue(allSameA.same(emptyArray));
	}
	
	@Test(expected=Exception.class)
	public void notEmptyB() throws Exception {
		assertTrue(allSameB.same(emptyArray));
	}
	
	@Test(expected=Exception.class)
	public void notEmptyC() throws Exception {
		assertTrue(allSameC.same(emptyArray));
	}
	
	@Test(expected=Exception.class)
	public void negativeArrayA() throws Exception {
		assertTrue(allSameA.same(negArray));
	}
	
	@Test(expected=Exception.class)
	public void negativeArrayB() throws Exception {
		assertTrue(allSameB.same(negArray));
	}
	
	@Test(expected=Exception.class)
	public void negativeArrayC() throws Exception {
		assertTrue(allSameC.same(negArray));
	}
	
	@Test
	public void manyNumArrayA() throws Exception {
		assertTrue(allSameA.same(manyArrayTrue));
	}
	
	@Test
	public void manyNumArrayB() throws Exception {
		assertTrue(allSameB.same(manyArrayTrue));
	}
	
	@Test
	public void manyNumArrayC() throws Exception {
		assertTrue(allSameC.same(manyArrayTrue));
	}
	
	@Test
	public void manyArrayFalseA() throws Exception {
		assertFalse(allSameA.same(manyArrayFalse));
	}
	
	@Test
	public void manyArrayFalseB() throws Exception {
		assertFalse(allSameB.same(manyArrayFalse));
	}
	
	@Test
	public void manyArrayFalseC() throws Exception {
		assertFalse(allSameC.same(manyArrayFalse));
	}
	
	@Test
	public void boundaryBeginA() throws Exception {
		assertFalse(allSameA.same(boundaryBegin));
	}
	
	@Test
	public void boundaryBeginB() throws Exception {
		assertFalse(allSameB.same(boundaryBegin));
	}
	
	@Test
	public void boundaryBeginC() throws Exception {
		assertFalse(allSameC.same(boundaryBegin));
	}
	
	@Test
	public void boundaryEndA() throws Exception {
		assertFalse(allSameA.same(boundaryEnd));
	}
	
	@Test
	public void boundaryEndB() throws Exception {
		assertFalse(allSameB.same(boundaryEnd));
	}
	
	@Test
	public void boundaryEndC() throws Exception {
		assertFalse(allSameC.same(boundaryEnd));
	}
	
	@Test
	public void manyNormalA() throws Exception {
		assertFalse(allSameA.same(manyNormal));
	}
	
	@Test
	public void manyNormalB() throws Exception {
		assertFalse(allSameB.same(manyNormal));
	}
	
	@Test
	public void manyNormalC() throws Exception {
		assertFalse(allSameC.same(manyNormal));
	}

}
